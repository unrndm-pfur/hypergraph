from __future__ import annotations

from copy import copy
from json import dumps, loads
from typing import (
    Any,
    Generic,
    NoReturn,
    Optional,
    TypedDict,
    TypeVar,
    Union,
    overload,
)
from collections.abc import Iterable
from itertools import permutations


class Vertex:
    def __init__(self, name: str):
        """Constructor of Vertex

        Parameters
        ----------
        name : str
            name of edge
        """
        self.name = name

    def __str__(self):
        return f"Vertex `{self.name}`"

    def __repr__(self):
        return f"Vertex('{self.name}')"

    def is_incident(self, edge: Edge):
        return self in edge.vertices

    def __hash__(self) -> int:
        return hash((self.name))

    def __eq__(self, value: Vertex) -> bool:
        return self.name == value.name


class Edge:
    def __init__(
        self,
        vertices: Iterable[Vertex],
    ):
        """Constructor of Edge

        Parameters
        ----------
        vertices : Iterable[Vertex]
            set of vertices at the beginning
        """
        vertices = set(vertices)
        if len(vertices) == 0:
            raise ValueError("Edge's vertices can't be empty")
        self.vertices = vertices

    def is_loop(self) -> bool:
        return len(self.vertices) == 1

    def is_incident(self, vertex: Vertex):
        return vertex in self.vertices

    def __contains__(self, vertex: Vertex) -> boold:
        return is_incident(vertex)

    # смежность
    def are_adjacent(self, edge: Edge) -> bool:
        return len(self.vertices & edge.vertices) > 0

    def is_weighted(self) -> bool:
        return self.__is_weighted__

    # re implement

    def __is_weighted__(self) -> bool:
        return False

    def __and__(self, edge: Edge) -> bool:
        return self.are_adjacent(edge)

    def __hash__(self) -> int:
        return hash((frozenset(self.vertices)))

    def __eq__(self, value: Edge) -> bool:
        return self.vertices == value.vertices

    def __str__(self):
        return f"Edge `{self.vertices}`"

    def __repr__(self):
        return f"Edge({self.vertices})"


T = TypeVar("T")


class WeightedEdge(Edge, Generic[T]):
    def __init__(
        self,
        vertices: Iterable[Vertex],
        weight: T,
    ):
        """Constructor of Edge

        Parameters
        ----------
        vertices : list[Vertex]
            list of vertices at the beginning
        weight: T
            weights, can be any type
        """
        vertices = set(vertices)
        if len(vertices) == 0:
            raise ValueError("Edge's vertices can't be empty")
        self.vertices = vertices.copy()
        self.weight = copy(weight)

    # re-implemented

    def __is_weighted__(self) -> bool:
        return True

    def __hash__(self) -> int:
        return hash((frozenset(self.vertices), self.weight))

    def __eq__(self, value: Union[Edge, WeightedEdge]) -> bool:
        if isinstance(value, Edge):
            return self.vertices == value.vertices
        elif isinstance(value, WeightedEdge):
            return (self.vertices == value.vertices) and (self.weight == value.weight)
        else:
            raise ValueError(f"unknown type {type(value)}")

    def __str__(self):
        return f"WeightedEdge[{self.weight}] `{self.vertices}`"

    def __repr__(self):
        return f"WeightedEdge({self.vertices}, {self.weight})"


class Hypergraph:
    def __init__(
        self,
        vertices: Iterable[Vertex],  # вершины
        edges: Iterable[Edge],  # рёбра
    ):
        self.vertices = set(vertices)
        self.edges = set(edges)

    def __str__(self) -> str:
        return f"Hyperraph {len(self.vertices)}V->{len(self.edges)}E"

    def __repr__(self) -> str:
        return f"Hyperraph(vertices={self.vertices}, edeges={self.edges})"

    class SerializedGraph(TypedDict):
        vertices: set[Vertex]
        edges: set[Edge]

    @classmethod
    def from_dict(cls, sg: Hypergraph.SerializedGraph) -> Hypergraph:
        graph = cls(sg["vertices"], sg["edges"])
        return graph

    def to_dict(self) -> SerializedGraph:
        return {
            "vertices": self.vertices,
            "edges": self.edges,
        }

    @classmethod
    def from_json_str(cls, json_str: str) -> Hypergraph:
        loaded_json: Hypergraph.SerializedGraph = loads(json_str)
        return cls.from_dict(loaded_json)

    def to_json_str(self) -> str:
        return dumps(self.to_dict())

    def add_vertex(self, vertex: Vertex):
        """
        add vertex to inner set of vertices

        Parameters
        ----------
        vertex : Vertex

        Notes
        -----
        doesn't raise key error if vertex already exists
        """
        self.vertices.add(vertex)

    def add_edge(self, edge: Edge):
        """
        add edge to inner set of edges

        Parameters
        ----------
        edge : Edge

        Notes
        -----
        doesn't raise key error if edge already exists
        """
        for vertex in edge.vertices:
            if vertex not in self.vertices:
                raise ValueError(
                    f"{edge=} connects {vertex=}, which doesn't exist on this hypergraph"
                )
        self.edges.add(edge)

    def clear(self):
        self.vertices = set()
        self.edges = set()

    # смежность
    @overload
    def are_adjacent(self, element_a: Vertex, element_b: Vertex) -> bool:
        ...

    @overload
    def are_adjacent(self, element_a: Edge, element_b: Edge) -> bool:
        ...

    def are_adjacent(
        self, element_a: Union[Vertex, Edge], element_b: Union[Vertex, Edge]
    ) -> bool:
        """
        checks if vertices are adjacent

        Parameters
        ----------
        element_a : Union[Vertex, Edge]
        element_b : Union[Vertex, Edge]

        Returns
        -------
        bool
            True if vertices are adjacent, False otherwise
        """
        if isinstance(element_a, Vertex) and isinstance(element_b, Vertex):
            for e in self.edges:
                if (element_a in e.vertices) and (element_b in e.vertices):
                    return True
            return False
        elif isinstance(element_a, Edge) and isinstance(element_b, Edge):
            if len(element_a.vertices & element_b.vertices) > 0:
                return True
            else:
                return False
        else:
            raise ValueError(
                f"Unknown type combination for variables "
                f"{element_a=} and {element_b=}"
            )

    # мульти
    def is_multiple(self, edge: Edge) -> bool:
        for ge in self.edges:
            if edge.vertices == ge.vertices:
                return True
        return False

    def get_multi(self) -> set[Edge]:
        return {e for e in self.edges if self.is_multiple(e)}

    def is_multi(self) -> bool:
        return len(self.get_multi()) > 1

    def get_loops(self) -> set[Edge]:
        return {e for e in self.edges if e.is_loop()}

    def is_simple(self) -> bool:
        return (len(self.get_loops()) == 0) and (len(self.get_multi()) == 0)

    @overload
    def get_incident(self, element: Vertex) -> set[Edge]:
        ...

    @overload
    def get_incident(self, element: Edge) -> set[Vertex]:
        ...

    def get_incident(
        self, element: Union[Vertex, Edge]
    ) -> Union[set[Edge], set[Vertex]]:
        if isinstance(element, Vertex):
            return {e for e in self.edges if element.is_incident(e)}
        elif isinstance(element, Edge):
            return element.vertices
        else:
            raise ValueError(f"Unknown instance {type(element)}")

    @overload
    def get_degree(self, element: Vertex) -> int:
        ...

    @overload
    def get_degree(self, element: Edge) -> int:
        ...

    def get_degree(self, element: Union[Vertex, Edge]) -> int:
        if isinstance(element, Vertex):
            return len(self.get_incident(element))
        elif isinstance(element, Edge):
            return len(element.vertices)
        else:
            raise ValueError(f"Unknown instance {type(element)}")

    def get_isolated(self) -> set[Vertex]:
        not_isolated: set[Vertex] = set()
        for e in self.edges:
            not_isolated.update(e.vertices)
        return self.vertices - not_isolated

    # изоморфность
    def is_incident(self, other: Hypergraph) -> bool:
        return (self.vertices == other.vertices) and (self.edges == other.edges)

    def __eq__(self, other: Hypergraph) -> bool:
        return self.is_incident(other)

    # реберный подгиперграф
    def is_edge_subgraph(self, other: Hypergraph) -> bool:
        return self.edges.issubset(other.edges)

    def __lt__(self, other: Hypergraph) -> bool:
        """
        checks if self is edge subgraph of other

        Parameters
        ----------
        other : Hypergraph

        Returns
        -------
        bool
            True if self graph is edge subgraph of other hypergraph
        """
        return self.is_edge_subgraph(other)

    # подгиперграф
    def is_subgraph(self, other: Hypergraph) -> bool:
        return self.is_edge_subgraph(other) and self.vertices.issubset(other.vertices)

    def __le__(self, other: Hypergraph) -> bool:
        """
        checks if self is subgraph of other

        Notes
        -----
        full subgraph, i.e. edge subgraph and vertices subgraph

        Parameters
        ----------
        other : Hypergraph

        Returns
        -------
        bool
            True if self graph is subgraph of other hypergraph
        """
        return self.is_subgraph(other)

    class HypergraphCombinations(TypedDict):
        edges: set[Edge]
        is_maximal: bool
        is_minimal: bool
        is_perfect: bool

    # сочетание
    def get_combinations(self) -> HypergraphCombinations:
        # TODO: read literature bc I have no idea how to do this
        raise NotImplementedError

    # однородность
    @property
    def uniformity(self) -> Optional[int]:
        """
        uniformity property for graph uniformity

        Returns
        -------
        Optional[int]
            None if hypergraph doesn't have uniformity or has edges that are multiple, int - otherwise
        """
        if self.is_multiple():
            return None

        degree: set[int] = {self.get_degree(v) for v in self.vertices}
        if len(degree) == 1:  # длина множества значений степеней рёбер равна 1
            return degree[0]
        else:
            return None

    @uniformity.setter
    def uniformity(self, value: Any) -> NoReturn:
        """
        property setter, shouldn't be used

        Parameters
        ----------
        value : Any

        Raises
        ------
        ValueError
            can't set uniformity property
        """
        raise ValueError("can't set uniformity property")

    # дольность
    @property
    def fractional(self) -> Optional[int]:
        """
        property for graph fractionality

        Returns
        -------
        Optional[int]
            None if hypergraph doesn't have fractionality, int - otherwise
        """
        raise NotImplementedError
        v_lens: set[int] = {len(e.vertices) for e in self.edges}
        if len(v_lens) != 1:
            raise ValueError("number of vertices in edges isn't consistent")
        else:
            segment_length = v_lens[0]
            num_segments = len(self.vertices) / segment_length

            for perm in permutations(self.vertices):
                edges: list[set[Vertices]] = []
                for i in range(num_segments):
                    edges.append(
                        set(perm[i * segment_length : (i + 1) * segment_length])
                    )

                next_perm = False
                for e in edges:
                    for p2 in perm(edges, 2):
                        if self.are_adjacent(*p2):
                            next_perm = True
                            break
                    if next_perm:
                        break
                if next_perm:
                    continue

                found_edge: list[bool] = []
                for e1 in edges:
                    found_edge.append(False)
                    for e2 in self.edges:
                        if e1 == e2.vertices:
                            found_edge[-1] = True
                            break

                if not all(found_edge):
                    continue
                else:
                    pass

    @fractional.setter
    def fractional(self, value: Any) -> NoReturn:
        """
        fractionality property setter, shouldn't be used

        Parameters
        ----------
        value : Any

        Raises
        ------
        ValueError
            can't set fractionality property
        """
        raise ValueError("can't set fractionality property")

    class Star(TypedDict):
        center: Vertex
        edges: set[Edge]
        degree: int
        is_simple: bool

    def get_stars(self) -> list[Star]:
        stars: list[Star] = []
        for vertex in self.vertices:
            stars.append(
                {"center": vertex, "edges": set(), "degree": 0, "is_simple": False}
            )
            for edge in self.edges:
                if vertex in edge:
                    stars[-1]["edges"].add(edge)
                    stars[-1]["degree"] += 1
            if len(set.intersection(e.vertices for e in stars[-1]["edges"])) == 1:
                stars[-1]["is_simple"] = True
        return stars

    def is_weighted(self) -> bool:
        return all(e.is_weighted for e in self.edges)
