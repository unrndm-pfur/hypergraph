from __future__ import annotations

from collections.abc import Iterable
from functools import reduce
from itertools import product, cycle
from typing import Optional
from copy import deepcopy

from .hypergraph import Edge, Hypergraph
from .util import transpose


class Solver:
    @staticmethod
    def group_criteria(hg: Hypergraph) -> list[list[float]]:
        """преобразовывает входные данные - вершины рёбер графа

        Parameters
        ----------
        hg : Hypergraph
            гиппергарф

        Returns
        -------
        list[list[float]]
            преобразованые данные
        """
        assert hg.is_weighted, "гиперграф должен быть взвешеным"

        w: List[List[float]] = [[w for w in edge.weight] for edge in hg.edges]
        print(w)
        w_T = transpose(w)

        f: tuple[list[flaot]] = [None for _ in range(len(w_T))]
        f[0] = [
            -1 * ((w_T_i - min(w_T[0])) / (max(w_T[0]) - min(w_T[0])))
            for w_T_i in w_T[0]
        ]
        for i in range(1, len(w_T)):
            f[i] = [
                ((w_T_i - min(w_T[i])) / (max(w_T[i]) - min(w_T[i])))
                for w_T_i in w_T[i]
            ]
        return transpose(f)

    @staticmethod
    def uniform_optimization_method(
        inp: Iterable[Iterable[float]],
    ) -> tuple[float, ...]:
        """Метод равномерной оптимизации

        Parameters
        ----------
        inp : Iterable[Iterable[float]]
            псевдо матрица, хранящая альтернативы и цели/критерии

        Returns
        -------
        tuple[float, ...]
            критерии, длинна равна количеству альтернатив
        """
        return tuple((sum(a) for a in inp))

    @staticmethod
    def fair_compromise_method(inp: Iterable[Iterable[float]]) -> tuple[float, ...]:
        """Метод справедливого компромисса

        Parameters
        ----------
        inp : Iterable[Iterable[float]]
            псевдо матрица, хранящая альтернативы и цели/критерии

        Returns
        -------
        tuple[float, ...]
            критерии, длинна равна количеству альтернатив
        """
        inp_T = transpose(inp)
        inp_T[0] = [i + 1 for i in inp_T[0]]
        inp = transpose(inp_T)
        return tuple((reduce(lambda x, y: x * y, a, initializer=1) for a in inp))

    @staticmethod
    def rolling_criteria_method(
        inp: Iterable[Iterable[float]], parameters: Iterable[float]
    ) -> tuple[float, ...]:
        """Метод свертывания критериев

        Parameters
        ----------
        inp : Iterable[Iterable[float]]
            псевдо матрица, хранящая альтернативы и цели/критерии
        parameters : Iterable[float]
            итерабельный объект параметров

        Returns
        -------
        tuple[float, ...]
            критерии, длинна равна количеству альтернатив
        """
        return tuple(
            (
                reduce(
                    lambda x, idx_y: x * idx_y[1] * parameters[idx_y[0]],
                    enumerate(a),
                    initializer=1,
                )
                for a in inp
            )
        )

    @staticmethod
    def main_criteria_method(inp: Iterable[Iterable[float]], d: float) -> int:
        """Метод главного критерия

        Parameters
        ----------
        inp : Iterable[Iterable[float]]
            псевдо матрица, хранящая альтернативы и цели/критерии
        d : float
            заданый параметр d

        Returns
        -------
        int
            найденая альтернитива
        """
        res: Optional[int] = None
        for idx, a in enumerate(inp):
            if res in None:
                if all([a_i >= d for a_i in a[1:]]):
                    res = idx
            else:
                if (a[0] > inp[res][0]) and all([a_i >= d for a_i in a[1:]]):
                    res = idx
        return res

    @staticmethod
    def ideal_poit_method_sevidge_principle(inp: Iterable[Iterable[float]]) -> float:
        """Метод идеальной точки, принцип Сэвиджа

        Parameters
        ----------
        inp : Iterable[Iterable[float]]
            псевдо матрица, хранящая альтернативы и цели/критерии

        Returns
        -------
        float
            найденая точка
        """
        w_T = transpose(inp)
        w = [[max(col) - el for el in col] for col in w_T]
        w = transpose(w)

        new_col = [max(row) for row in w]
        res = min(new_col)
        return res

    @staticmethod
    def food_ration(
        prod2element_amount: Iterble[Iterable[float]],  # a[, ]
        requirment: Iterable[float],  # b[]
        costs: Iterable[float],  # c[]
        /,
        *,
        min_max: tuple[int, int] = (-1, 1),
        accuracy: float = 0.1,
    ) -> Optional[list[float]]:
        prod2element_amount: list[list[float]] = [
            [el_am for el_am in prod] for prod in prod2element_amount
        ]
        requirment: list[float] = list(requirment)
        costs: list[float] = list(costs)

        num_prod = len(costs)

        xs: list[float] = [
            min_max[0] + (i * accuracy)
            for i in range(1 + round((min_max[1] - min_max[0]) / accuracy))
        ]
        # xs = [min_max[0]]
        # while xs[-1] < min_max[1]:
        # xs.append(xs[-1]+accuracy)

        L_max: Optional[float] = None
        X_max: Optional[list[float]] = None

        for x in product(xs, repeat=num_prod):
            # print(x)
            cond: bool = all(
                [
                    sum([el_am * x[el_i] for el_i, el_am in enumerate(prod)])
                    >= requirment[prod_i]
                    for prod_i, prod in enumerate(prod2element_amount)
                ]
            )
            if cond:
                L: float = sum([costs[i] * x_i for i, x_i in enumerate(x)])
                if (L_max is None) or (L > L_max):
                    L_max = L
                    X_max = x
        return X_max

    @staticmethod
    def production_planning(
        prod2resource: Iterble[Iterable[float]],  # a[, ]
        requirment: Iterable[float],  # b[]
        profit: Iterable[float],  # c[]
        maxs: Iterable[float],  # beta[]
        max_res: Iterable[float],  # gamma[]
        /,
        *,
        min_max: tuple[int, int] = (-1, 1),
        accuracy: float = 0.1,
    ) -> Optional[list[float]]:
        prod2resource: list[list[float]] = [
            [res for res in prod] for prod in prod2resource
        ]
        requirment: list[float] = list(requirment)
        profit: list[float] = list(profit)
        maxs: list[float] = list(maxs)
        max_res: list[float] = list(max_res)

        num_prod = len(requirment)

        xs: list[float] = [
            min_max[0] + (i * accuracy)
            for i in range(1 + round((min_max[1] - min_max[0]) / accuracy))
        ]

        L_max: Optional[float] = None
        X_max: Optional[list[float]] = None

        for x in product(xs, repeat=num_prod):
            # print(x)
            cond: bool = (
                all([x >= requirment[i] for i, x_i in enumerate(x)])
                and all([x <= maxs[i] for i, x_i in enumerate(x)])
                and all(
                    [
                        sum([res * x[res_i] for res_i, res in enumerate(prod)])
                        <= max_res[prod_i]
                        for prod_i, prod in enumerate(prod2resource)
                    ]
                )
            )
            if cond:
                L: float = sum([profit[i] * x_i for i, x_i in enumerate(x)])
                if (L_max is None) or (L > L_max):
                    L_max = L
                    X_max = x
        return X_max

    @staticmethod
    def equipment_loading(
        type1_num: int,  # N1
        type2_num: int,  # N2
        machine2fabric: Iterable[iterable[float]],  # a(N2T)[, ]
        mins: Iterable[float],  # b[]
        profit: Iterable[float],  # c[]
        maxs: Iterable[float],  # betta[]
        /,
        *,
        min_max1: tuple[int, int] = (-1, 1),
        min_max2: tuple[int, int] = (-1, 1),
        accuracy1: float = 0.1,
        accuracy2: float = 0.1,
    ) -> tuple[Optional[list[float]], Optional[list[float]]]:
        machine2fabric: list[list[float]] = [
            [fabric for fabric in machine] for machine in machine2fabric
        ]
        mins: list[float] = list(mins)
        profit: list[float] = list(profit)
        maxs: list[float] = list(maxs)

        num_fabric = len(mins)

        xs1: list[float] = [
            min_max1[0] + (i * accuracy1)
            for i in range(1 + round((min_max1[1] - min_max1[0]) / accuracy1))
        ]
        xs2: list[float] = [
            min_max2[0] + (i * accuracy2)
            for i in range(1 + round((min_max2[1] - min_max2[0]) / accuracy2))
        ]

        L_max: Optional[float] = None
        X1_max: Optional[list[float]] = None
        X2_max: Optional[list[float]] = None

        for x1, x2 in zip(
            product(xs1, repeat=num_fabric), product(xs2, repeat=num_fabric)
        ):
            # print(x1, x2)
            cond: bool = (
                all(
                    [
                        maxs[i]
                        >= sum(
                            [ai[0] * x1_i + x2_i * ai[1] for ai in machine2fabric[i]]
                        )
                        >= mins[i]
                        for i, (x1_i, x2_i) in enumerate(zip(x1, x2))
                    ]
                )
                and (sum(x1) == type1_num)
                and (sum(x2) == type2_num)
            )
            if cond:
                L: float = sum(
                    [
                        c_i
                        * (
                            [
                                machine2fabric[i][0] * x1[i]
                                + machine2fabric[i][1] * x2[i]
                            ]
                        )
                        for i, c_i in enumerate(profit)
                    ]
                )
                if (L_max is None) or (L > L_max):
                    L_max = L
                    X1_max = x1
                    X2_max = x2
        return X1_max, X2_max

    @staticmethod
    def material_supply(
        num_factories: int,
        requirments: iterable[float],  # a[] len of num_factories
        capabilities: Iterable[float],  # b[]
        factory2base: Iterable[Iterable[float]],  # c[, ]
        /,
        *,
        min_max: Iterable[tuple[int, int]] = None,
        accuracy: Iterable[float] = None,
    ) -> Optional[list[list[float]]]:
        requirments: list[float] = list(requirments)
        capabilities: list[float] = list(capabilities)
        factory2base: list[list[float]] = [
            [factory for factory in base] for base in factory2base
        ]

        if min_max is None:
            min_max: Iterable[tuple[int, int]] = [(-1, 1) for _ in num_factories]
        if accuracy is None:
            accuracy: Iterable[float] = [0.1 for _ in num_factories]

        xss: list[list[float]] = [
            [
                min_max[nf] + (i * accuracy[nf])
                for i in range(1 + round((min_max[nf] - min_max[nf]) / accuracy[nf]))
            ]
            for nf in range(num_factories)
        ]

        L_min: Optional[float] = None
        Xs_min: Optional[list[list[float]]] = None

        for xs in zip(*[product(xs, repeat=num_factories) for xs in xss]):
            print(xs)
            cond: bool = (
                all([sum(xs_i) == requirments[i] for i, xs_i in enumerate(xs)]) and
                all([sum(xs_i) <= capabilities[i] for i, xs_i in enumerate(transpose(xs))])
            )
            if cond:
                L: float = sum(
                    [sum([xs_ij*factory2base[i][j] for j, xs_ij in enumerate(xs_i)]) for i, xs_i in enumerate(xs)]
                )
                if (L_min is None) or (L > L_min):
                    L_min = L
                    Xs_min = xs
        return Xs_min
    
    @staticmethod
    def transport_task(
        a: Iterable[float], # отправление
        b: Iterable[float], # назначение
        c: Iterable[Iterable[float]], # стоимость
        optimizing_method: str = "minimal",
    ) -> float:
        a: list[float] = list(a)
        m = len(a)

        b: list[float] = list(b)
        n = len(b)

        c: list[list[float]] = [[j for j in i] for i in c]

        assert (len(c) == m) and (len(c[0]) == n)
        assert sum(a) == sum(b), "10.1"

        a_copy: list[float] = a.copy()
        a_copy.reverse()
        b_copy: list[float] = b.copy()
        b_copy.reverse()
        x = [[0 for _ in range(n)] for _ in range(m)]
        while sum(b_copy) > 0:
            if b_copy[-1] > a_copy[-1]:
                x[m-len(a_copy)][n-len(b_copy)] = a_copy[-1]
                b_copy[-1] -= a_copy[-1]
                a_copy.pop()
            elif b_copy[-1] == a_copy[-1]:
                x[m-len(a_copy)][n-len(b_copy)] = a_copy[-1]
                a_copy.pop()
                b_copy.pop()
            elif b_copy[-1] < a_copy[-1]:
                x[m-len(a_copy)][n-len(b_copy)] = b_copy[-1]
                a_copy[-1] -= b_copy[-1]
                b_copy.pop()
        del a_copy, b_copy
        print(x)

        # if optimizing_method == "minimal":
        #     min_c = list(sorted(
        #         [((i, j), cij)  for i, ci in enumerate(c) for j, cij in enumerate(ci)],
        #         key=lambda el: el[1],
        #     ))
        #     for (a, b), mc in min_c:
        #         print("-"*10)
        #         print(a, b)
        #         cycle = []
        #         new_point = a, b
        #         cycle.append(new_point)
        #         if new_point[0] < new_point[1]:
        #             print("right upper")
        #             ns = list(range(b-1, -1, -1))
        #         else:
        #             print("left lover or center")
        #             ns = list(range(b+1, n))
        #         for j in ns:
        #             if x[new_point[0]][j] > 0:
        #                 new_point = new_point[0], j
        #                 break
        #         cycle.append(new_point)
        #         if a < b:
        #             print("upper right")
        #             ms = list(range(b-1, -1, -1))
        #         else:
        #             print("lover left or center")
        #             ms = list(range(b+1, n))
        #         for i in ms:
        #             if x[i][new_point[1]] > 0:
        #                 new_point = i, new_point[1]
        #                 break
        #         cycle.append(new_point)

        #         while new_point != (a, b):
        #             if new_point[0] < new_point[1]:
        #                 print("right upper")
        #                 ns = list(range(b-1, -1, -1))
        #             else:
        #                 print("left lover or center")
        #                 ns = list(range(b+1, n))
        #             for j in ns:
        #                 if x[new_point[0]][j] > 0:
        #                     new_point = new_point[0], j
        #                     break
        #             cycle.append(new_point)
        #             if a < b:
        #                 print("upper right")
        #                 ms = list(range(b-1, -1, -1))
        #             else:
        #                 print("lover left or center")
        #                 ms = list(range(b+1, n))
        #             for i in ms:
        #                 if x[i][new_point[1]] > 0:
        #                     new_point = i, new_point[1]
        #                     break
        #             cycle.append(new_point)
        #         print(cycle)
        #     print(min_c)

        L: float = sum([sum([c*x for (c,x) in zip(*cx_i)]) for cx_i in zip(c, x)])
        return L


