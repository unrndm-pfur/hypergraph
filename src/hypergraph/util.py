from collections.abc import Iterable
from typing import Any


def transpose(inp: Iterable[Iterable[Any]]) -> list[list[Any]]:
    return [list(i) for i in zip(*inp)]
