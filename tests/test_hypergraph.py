import pytest

from hypergraph import Edge, Hypergraph, Vertex, __version__
from hypergraph.__main__ import main


@pytest.fixture
def graph() -> Hypergraph:
    v = {"a": Vertex("a"), "b": Vertex("b"), "c": Vertex("c"), "d": Vertex("d")}
    e = [Edge([v["a"], v["b"]])]
    g = Hypergraph(list(v.values()), e)

    return g


def test_idk():
    """idk what to do with this"""
    main()


def test_version():
    assert __version__ == "0.1.0"


def test_edge_constructor_raises_error():
    with pytest.raises(ValueError):
        Edge([])


def test_add_vertex(graph: Hypergraph):
    vertex = Vertex("e")
    graph.add_vertex(vertex)
    assert vertex in graph.vertices


# def test_add_edge(graph: Hypergraph):
# edge = Edge([graph.vertices[1], graph.vertices[2]])
# graph.add_edge(edge)
# assert edge in graph.edges


# def test_add_edge_raises_error(graph: Hypergraph):
# with pytest.raises(ValueError):
# edge = Edge([Vertex("z")])
# graph.add_edge(edge)


# def test_is_incident(graph: Hypergraph):
# assert graph.vertices[0].is_incident(graph.edges[0]) is True
# assert graph.vertices[3].is_incident(graph.edges[0]) is False


# def test_get_incident(graph: Hypergraph):
# assert graph.get_incident(graph.vertices[0]) == [graph.edges[0]]
# assert graph.get_incident(graph.vertices[3]) == []


# def test_get_degree(graph: Hypergraph):
# assert graph.get_degree(graph.vertices[0]) == 1
# assert graph.get_degree(graph.vertices[3]) == 0
# assert graph.get_degree(graph.edges[0]) == 2


def test_get_degree_raises_error(graph: Hypergraph):
    with pytest.raises(ValueError):
        graph.get_degree(None)  # noqa
