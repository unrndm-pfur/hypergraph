import pytest

from hypergraph import Hypergraph, Vertex, WeightedEdge
from hypergraph.others import Solver as OtherSolver


@pytest.fixture
def graph() -> Hypergraph:
    v = [Vertex("a")]
    e = [
        WeightedEdge([v[0]], (7, 45, 90)),
        WeightedEdge([v[0]], (5, 40, 85)),
        WeightedEdge([v[0]], (9, 50, 80)),
        WeightedEdge([v[0]], (6, 45, 83)),
    ]
    g = Hypergraph(v, e)
    g = [
        (7, 45, 90),
        (5, 40, 85),
        (9, 50, 80),
        (6, 45, 83),
    ]
    return g


# def test_transform(graph: Hypergraph):
#     res = OtherSolver.group_criteria(graph)
#     expected = [
#         [-0.5 , 0.5, 1.0],
#         [ 0.0 , 0.0, 0.5],
#         [-1.0 , 1.0, 0.0],
#         [-0.25, 0.5, 0.3],
#     ]
#     print(res)
#     assert res == expected
